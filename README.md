# Archweb Retro pages

This repository contains old Archweb webarchives ranging from 2002-2013.

These pages where obtained using wget -r on https://archlinux.org and editing
the obtained index.html to no longer reference to "/static" and moving the assets
to the $year directory.

## Viewing pages

Viewing the retro pages is as simple as running a simple python httpserver

```
python -m http.server
```
